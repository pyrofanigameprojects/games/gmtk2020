﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class PlayerRotationScript : MonoBehaviour
{
    private Vector2 mousePosition;
    private float playerRads;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       mousePosition=(Camera.main.WorldToScreenPoint(Input.mousePosition)-transform.position);
        findRadOfMousePositionTurnItToDegreesAndRotate();
    }
    private void findRadOfMousePositionTurnItToDegreesAndRotate()
    {
        playerRads=Mathf.Atan2(mousePosition.y, mousePosition.x);
        transform.localRotation=  Quaternion.Euler(0.0f,0.0f,playerRads*Mathf.Rad2Deg*2);
    }
}
